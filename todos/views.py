from django.shortcuts import render, redirect
from todos.models import TodoList
from todos.forms import TodoListForm

# Create your views here.


def todo_list_list(request):
    todolists = TodoList.objects.all()
    context = {
        "todolists_object": todolists,
    }
    return render(request, "todolist/tdlists.html", context)


def todo_list_details(request, id):
    tododetails = TodoList.objects.get(id=id)
    context = {
        "tododetails_object": tododetails
    }
    return render(request, "todolist/details.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo = form.save(False)
            todo.save()
            return redirect("Todo")
