from django.urls import path
from todos.views import todo_list_list, todo_list_details


urlpatterns = [
    path("", todo_list_list, name="todo_list_list"),
    path("<int:id>/", todo_list_details, name="todo_list_details"),
]
